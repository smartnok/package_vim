# 1.0.0 (2021-08-29)


### Bug Fixes

* bite ([b1a4981](https://gitlab.com/smartnok/package_vim/commit/b1a4981ce5e295f1108ba7de81375a9adf5c9401))


### Features

* test another release ([393cb35](https://gitlab.com/smartnok/package_vim/commit/393cb353d795fd23158f24b8cdc1b6a1614bce0b))
* **zeb:** oklm mec ([818a9a6](https://gitlab.com/smartnok/package_vim/commit/818a9a692ab7ebfbf855329791d157325611d220))
