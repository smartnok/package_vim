import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_vim_installed(host):
    assert host.package("vim-common").is_installed

def test_vimrc_file_exists(host):
    with host.sudo():
        assert host.file("/root/.vimrc").exists

def test_user_vagrant_exists(host):
    assert host.user("vagrant").exists
